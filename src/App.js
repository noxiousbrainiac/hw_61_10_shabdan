import React from 'react';
import CountryInfoApp from "./CountryInfoApp/CountryInfoApp";

const App = () => {
    return (
        <div>
          <CountryInfoApp/>
        </div>
    );
};

export default App;