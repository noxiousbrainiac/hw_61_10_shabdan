import React, {useEffect, useState} from 'react';
import axios from "axios";

const CountryInfo = ({country}) => {
    const [borders, setBorders] = useState([]);

    useEffect(() => {
        if (country.length !== 0) {
            const getBorderCountriesData = async () => {
                const promises =  country.borders.map(async item => {
                    const {data} = await axios.get(`https://restcountries.eu/rest/v2/alpha/${item}`);
                    return data;
                });
                const result = await Promise.all(promises)
                setBorders(result);
            }
            getBorderCountriesData();
        }
    },[country]);


    return (
        country.length !== 0 ?
        <div className="p-2">
            <h2>{country.name}</h2>
            <img src={country.flag} alt="flag" style={{width: "100px"}}/>
            <p><b>Region:</b> {country.region}</p>
            <p><b>Capital:</b> {country.capital}</p>
            <p><b>Population:</b> {country.population}</p>
            {borders.length !== 0 ?
                <div>
                    <p><b>Borders:</b></p>
                    <ul>
                        {borders.map(item => {
                            return <li key={item.alpha3Code}>{item.name}</li>
                        })}
                    </ul>
                </div> : null}
        </div> : null
    );
};

export default CountryInfo;