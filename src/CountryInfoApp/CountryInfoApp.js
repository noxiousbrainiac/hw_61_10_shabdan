import React, {useEffect, useState} from 'react';
import axios from "axios";
import CountriesList from "./CountriesList/CountriesList";
import '../assets/bootstrap.min.css';
import CountryInfo from "./CountryInfo/CountryInfo";

const url = 'https://restcountries.eu/rest/v2/all?fields=name;alpha3Code';

const CountryInfoApp = () => {
    const [countries, setCountries] = useState([]);
    const [country, setCountry] = useState([]);

    const getAxios = async () => {
        const data = await axios.get(url);
        setCountries(data.data);
    }

    const getCountry = async (code) => {
        const {data} = await axios.get(`https://restcountries.eu/rest/v2/alpha/${code}`);
        setCountry(data);
    }

    useEffect(() => {
        getAxios();
    }, []);

    return (
        <div className="container d-flex justify-content-evenly">
            <CountriesList countries={countries} getCountry={getCountry}/>
            <CountryInfo country={country}/>
        </div>
    );
};

export default CountryInfoApp;