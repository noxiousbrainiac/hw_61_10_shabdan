import React from 'react';
import '../../assets/bootstrap.min.css';
import CountryItem from "./CountryItem/CountryItem";

const CountriesList = ({countries, getCountry}) => {
    return (
        <div className="d-flex flex-column" style={{height: "730px", overflowY: "scroll"}}>
            {countries.map(item => {
                return <CountryItem
                    key={item.alpha3Code}
                    onItem={()=> getCountry(item.alpha3Code)}
                    name={item.name}
                />
            })}
        </div>
    );
};

export default CountriesList;